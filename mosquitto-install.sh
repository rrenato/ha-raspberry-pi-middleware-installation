#!/usr/bin/env bash

echo -e "###########################################################################################"

if [[ $# -ne 4 ]]
then
  echo "Sintassi: $(basename $0) <configuration file .ini> <influxdb admin passwd> <influxdb homeassistant passwd> <mosquitto homeassistant passwd>"
  exit 1
fi

export INIFILE=$1
export INFLUXDB_ADMIN_PASSWORD=$2
export INFLUXDB_HOMEASSISTANT_PASSWORD=$3

export MOSQUITTO_HOMEASSISTANT_PASSWORD=$4

if [[ -s $INIFILE ]] ; then
    . $INIFILE
    echo "# Loaded configuration file: $INIFILE"
else
    echo "ERRORE: file di inizializzazione $INIFILE non trovato oppure vuoto!"
    exit 1
fi

#----------------------------------------------------------------------------------------------------#
#                                                                                                    #
#                                              MOSQUITTO                                             #
#                                                                                                    #
#----------------------------------------------------------------------------------------------------#

./dockerc-pull.sh $INIFILE $INFLUXDB_ADMIN_PASSWORD $INFLUXDB_HOMEASSISTANT_PASSWORD $MOSQUITTO_HOMEASSISTANT_PASSWORD mosquitto
docker-compose build mosquitto

#----------------------------------------------------------------------------------------------------#
#                                                                                                    #
#                                       MOSQUITTO CLIENT                                             #
#                                                                                                    #
#----------------------------------------------------------------------------------------------------#
# get repo key
wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key

#add repo
sudo apt-key add mosquitto-repo.gpg.key

#download appropriate lists file
(cd /etc/apt/sources.list.d/ && sudo wget http://repo.mosquitto.org/debian/mosquitto-stretch.list)

#update caches and install
apt-cache search mosquitto
sudo apt-get update
sudo apt-get install -y libmosquitto-dev mosquitto-clients