#!/usr/bin/env bash

echo -e "###########################################################################################"

if [[ $# -ne 4 ]]
then
  echo "Sintassi: $(basename $0) <configuration file .ini> <influxdb admin passwd> <influxdb homeassistant passwd> <mosquitto homeassistant passwd>"
  exit 1
fi

export INIFILE=$1
export INFLUXDB_ADMIN_PASSWORD=$2
export INFLUXDB_HOMEASSISTANT_PASSWORD=$3

export MOSQUITTO_HOMEASSISTANT_PASSWORD=$4
export GLANCES_ADMIN_PASSWORD="none"

if [[ -s $INIFILE ]] ; then
    # shellcheck disable=SC1090
    . $INIFILE
    echo -e "$(date +"%Y-%m-%d %H-%M-%S") # Loaded configuration file: $INIFILE"
else
    echo "ERRORE: file di inizializzazione $INIFILE non trovato oppure vuoto!"
    exit 1
fi

# function to display menus
show_menus() {
	clear
	echo "Raspberry Pi"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo " U P G R A D E  -  M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo "1. Home Assistant    ($1 => $2)"
	echo "2. Eclipse Mosquitto ($3 => $4)"
	echo "3. Influxdb          ($5 => $6)"
	echo "4. Exit"
}

read_options(){
	local choice
	read -p "Enter choice [ 1 - 4] " choice
	case $choice in
		1) upgrade $HA_DOCKER_IMAGE "Home Assistant" homeassistant ;;
		2) upgrade $MOSQUITTO_IMAGE "Eclipse Mosquitto" mosquitto ;;
    3) upgrade $INFLUXDB_DOCKER_IMAGE "Influxdb" influxdb ;;
		4) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

upgrade(){
  DOCKER_IMAGE=$1
  DOCKER_IMAGE_DESCR=$2
  DOCKER_COMPOSE_NAME=$3

  DOCKER_PID=$(docker ps -a | grep $(echo "$DOCKER_IMAGE" | awk -F: '{ print $1 }') | awk '{ print $1 }')

  if [ -n "$DOCKER_PID" ] ;
  then
    echo -e "$(date +"%Y-%m-%d %H-%M-%S") removing '$DOCKER_IMAGE_DESCR' image id $DOCKER_PID from docker container"
    docker stop $DOCKER_PID 2>&1 >/dev/null ; sleep 7 ; docker container rm --force $DOCKER_PID 2>&1 >/dev/null
    echo -e "$(date +"%Y-%m-%d %H-%M-%S") removed '$DOCKER_IMAGE_DESC' image from docker container"
  fi

  DOCKER_PID=$(docker images --all | grep $(echo "$DOCKER_IMAGE" | awk -F: '{ print $1 }') | awk '{ print $3 }')

  if [ -n "$DOCKER_PID" ] ;
  then
    echo -e "$(date +"%Y-%m-%d %H-%M-%S") removing '$DOCKER_IMAGE_DESC' image id $DOCKER_PID from docker images"
    docker image rm --force $DOCKER_PID ; sleep 7
    echo -e "$(date +"%Y-%m-%d %H-%M-%S") removed '$DOCKER_IMAGE_DESC' image from docker images"
  fi

  docker image prune --force
  echo -e "$(date +"%Y-%m-%d %H-%M-%S") pull '$DOCKER_IMAGE_DESC' image"
  $DIR/dockerc-pull.sh $INIFILE $INFLUXDB_ADMIN_PASSWORD $INFLUXDB_HOMEASSISTANT_PASSWORD $MOSQUITTO_HOMEASSISTANT_PASSWORD $DOCKER_COMPOSE_NAME
  sleep 7
  echo -e "$(date +"%Y-%m-%d %H-%M-%S") build '$DOCKER_IMAGE_DESC' image"
  docker-compose build $DOCKER_COMPOSE_NAME
  sleep 7
  echo -e "$(date +"%Y-%m-%d %H-%M-%S") start '$DOCKER_IMAGE_DESC' image"
  docker-compose -f $DIR/docker-compose.yaml up -d $DOCKER_COMPOSE_NAME

  echo -e "# Start done."
  echo -e "####################################################################"
  exit 0
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

DOCKER_PS=$(docker ps -a 2>&1)
CURRENT_HA_V=$(echo -e "$DOCKER_PS" | grep $(echo "$HA_DOCKER_IMAGE" | awk -F: '{ print $1 }') | awk '{ print $2 }' | awk -F: '{ print $2 }')
CURRENT_MOSQUITTO_V=$(echo -e "$DOCKER_PS" | grep $(echo "$MOSQUITTO_IMAGE" | awk -F: '{ print $1 }') | awk '{ print $2 }' | awk -F: '{ print $2 }')
CURRENT_INFLUXDB_V=$(echo -e "$DOCKER_PS" | grep $(echo "$INFLUXDB_DOCKER_IMAGE" | awk -F: '{ print $1 }') | awk '{ print $2 }' | awk -F: '{ print $2 }')
CONF_HA_V=$(echo "$HA_DOCKER_IMAGE" | awk -F: '{ print $2 }')
CONF_MOSQUITTO_V=$(echo "$MOSQUITTO_IMAGE" | awk -F: '{ print $2 }')
CONF_INFLUXDB_V=$(echo "$INFLUXDB_DOCKER_IMAGE" | awk -F: '{ print $2 }')

while true
do
  show_menus $CURRENT_HA_V $CONF_HA_V $CURRENT_MOSQUITTO_V $CONF_MOSQUITTO_V $CURRENT_INFLUXDB_V $CONF_INFLUXDB_V
	read_options
done
