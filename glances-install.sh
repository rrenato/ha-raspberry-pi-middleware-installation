#!/usr/bin/env bash

echo -e "###########################################################################################"

if [[ $# -ne 3 ]]
then
  echo "Sintassi: $(basename $0) <configuration file .ini> <glances admin passwd> <influxdb admin passwd>"
  exit 1
fi

export INIFILE=$1
export GLANCES_ADMIN_PASSWORD=$2
export INFLUXDB_ADMIN_PASSWORD=$3

if [[ -s $INIFILE ]] ; then
    . $INIFILE
    echo "# Loaded configuration file: $INIFILE"
else
    echo "ERRORE: file di inizializzazione $INIFILE non trovato oppure vuoto!"
    exit 1
fi

echo -e "## Starting pull glances"
./dockerc-pull.sh $INIFILE "" "" "" glances


curl -X POST -G "http://localhost:8086/query?u=admin&p=$INFLUXDB_ADMIN_PASSWORD" --data-urlencode "q=CREATE USER \"glances\" WITH PASSWORD '$GLANCES_ADMIN_PASSWORD'"
curl -X POST -G "http://localhost:8086/query?u=admin&p=$INFLUXDB_ADMIN_PASSWORD" --data-urlencode "q=CREATE DATABASE \"glances\""
curl -X POST -G "http://localhost:8086/query?u=admin&p=$INFLUXDB_ADMIN_PASSWORD" --data-urlencode "q=GRANT ALL ON glances TO glances"

echo -e "## Putting up glances"
docker-compose up -d glances