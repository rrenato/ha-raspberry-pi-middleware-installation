#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y python3

sudo apt install -y python3-pip

sudo apt-get install -y python3-pil.imagetk

python3 -m pip install --user anki_vector
python3 -m pip install --user --upgrade anki_vector

sudo apt-get install libatlas-base-dev
sudo apt-get install -y python3-numpy

python3 -m anki_vector.configure
