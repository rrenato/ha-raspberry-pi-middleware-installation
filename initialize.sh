#!/usr/bin/env bash

echo -e "###########################################################################################"

#sudo apt-get install -y dnsmasq dnsutils ;
sudo apt-get install -y --reinstall build-essential

echo -e "# Configuring dnsmasq service"
sudo update-rc.d dnsmasq disable

echo -e "# Current configuration dnsmasq.conf"
grep -v ^# /etc/dnsmasq.conf | egrep -v ^$
sudo cp ./dnsmasq/dnsmasq.conf /etc/dnsmasq.conf
echo -e "# Current configuration resolv.dnsmasq"
grep -v ^# /etc/resolv.dnsmasq | egrep -v ^$
sudo cp ./dnsmasq/resolv.dnsmasq /etc/resolv.dnsmasq

sudo systemctl disable systemd-resolved
sudo systemctl stop systemd-resolved
sudo systemctl restart dnsmasq
# sudo systemctl status dnsmasq
sudo systemctl enable dnsmasq
sudo update-rc.d dnsmasq enable

echo -e "# Configuring git credential cache"
git config --global credential.helper cache
git config --global credential.helper 'cache --timeout=2800'
# git config --global --unset credential.helper #Disabling cache

# Install Node.js via Package Manager & Add Package Source
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -  # Install NodeJS v9
sudo apt-get install -y nodejs npm # nodejs-legacy #(Installed with nodesource)

if [ $(cat ~/.bashrc | grep "NODE_PATH" | wc -l) == 0 ]
then
    echo -e "export NODE_PATH=\$(npm root -g 2>/dev/null)" >>~/.bashrc
fi

npm install