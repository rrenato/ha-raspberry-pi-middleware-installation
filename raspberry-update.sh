#!/usr/bin/env bash

echo -e "###########################################################################################"

sudo locale-gen UTF-8
sudo raspi-config

sudo apt-get update ; sudo apt-get upgrade -y ; sudo apt-get install -y rpi-update ; sudo apt-get dist-upgrade -y
sudo apt-get clean ; sudo apt-get autoclean ; sudo apt-get autoremove

sleep 7 ; sudo reboot
