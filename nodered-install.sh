#!/usr/bin/env bash

echo -e "###########################################################################################"

if [[ $# -ne 1 ]]
then
  echo "Sintassi: $(basename $0) <configuration file .ini>"
  exit 1
fi

export INIFILE=$1
export GLANCES_ADMIN_PASSWORD=$2
export INFLUXDB_ADMIN_PASSWORD=$3

if [[ -s $INIFILE ]] ; then
    . $INIFILE
    echo "# Loaded configuration file: $INIFILE"
else
    echo "ERRORE: file di inizializzazione $INIFILE non trovato oppure vuoto!"
    exit 1
fi

echo -e "## Starting pull node-red"
./dockerc-pull.sh $INIFILE "" "" "" nodered

echo -e "## Putting up node-red"
docker-compose up -d nodered