#!/usr/bin/env bash

echo -e "###########################################################################################"

if [[ $# -ne 4 ]]
then
  echo "Sintassi: $(basename $0) <configuration file .ini> <influxdb admin passwd> <influxdb homeassistant passwd> <mosquitto homeassistant passwd>"
  exit 1
fi

export INIFILE=$1
export INFLUXDB_ADMIN_PASSWORD=$2
export INFLUXDB_HOMEASSISTANT_PASSWORD=$3

export MOSQUITTO_HOMEASSISTANT_PASSWORD=$4

if [[ -s $INIFILE ]] ; then
    . $INIFILE
    echo "# Loaded configuration file: $INIFILE"
else
    echo "ERRORE: file di inizializzazione $INIFILE non trovato oppure vuoto!"
    exit 1
fi

./dockerc-pull.sh $INIFILE $INFLUXDB_ADMIN_PASSWORD $INFLUXDB_HOMEASSISTANT_PASSWORD $MOSQUITTO_HOMEASSISTANT_PASSWORD homeassistant
docker-compose build homeassistant

