#!/usr/bin/env bash

echo -e "###########################################################################################"
echo -e "# zigbee2mqtt installer "
echo -e "###########################################################################################"

# Clone zigbee2mqtt repository
sudo git clone https://github.com/Koenkk/zigbee2mqtt.git /opt/zigbee2mqtt
sudo chown -R pirate:pirate /opt/zigbee2mqtt

# Install dependencies (as user "pi")
(cd /opt/zigbee2mqtt && npm install)

echo -e "####################################################################"
echo -e "# Install as service ..."
if [ ! -f /etc/systemd/system/zigbee2mqtt.service ] ;
then

sudo cat > /tmp/zigbee2mqtt.service <<'_text'
[Unit]
Description=zigbee2mqtt
After=network.target

[Service]
ExecStart=/usr/bin/npm start
WorkingDirectory=/opt/zigbee2mqtt
StandardOutput=inherit
StandardError=inherit
Restart=always
User=pirate

[Install]
WantedBy=multi-user.target
_text

sudo mv /tmp/zigbee2mqtt.service /etc/systemd/system/zigbee2mqtt.service
echo -e "# Install as service done"
fi

echo -e "# Install done."
echo -e "####################################################################"
