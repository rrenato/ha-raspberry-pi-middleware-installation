#!/usr/bin/env bash

echo -e "###########################################################################################"

if [[ $# -ne 4 ]]
then
  echo "Sintassi: $(basename $0) <configuration file .ini> <influxdb admin passwd> <influxdb homeassistant passwd> <mosquitto homeassistant passwd>"
  exit 1
fi

export INIFILE=$1
export INFLUXDB_ADMIN_PASSWORD=$2
export INFLUXDB_HOMEASSISTANT_PASSWORD=$3

export MOSQUITTO_HOMEASSISTANT_PASSWORD=$4

if [[ -s $INIFILE ]] ; then
    . $INIFILE
    echo "# Loaded configuration file: $INIFILE"
else
    echo "ERRORE: file di inizializzazione $INIFILE non trovato oppure vuoto!"
    exit 1
fi

mkdir -p ~/influxdb

echo -e "## Starting pull influxdb"
./dockerc-pull.sh $INIFILE $INFLUXDB_ADMIN_PASSWORD $INFLUXDB_HOMEASSISTANT_PASSWORD $MOSQUITTO_HOMEASSISTANT_PASSWORD influxdb

echo -e "## Generate e configure influxdb.conf"
docker run --rm influxdb influxd config > ~/influxdb/influxdb.conf
sed -i -e 's/auth-enabled = false/auth-enabled = true/g' ~/influxdb/influxdb.conf

echo -e "## Putting up influxdb"
docker-compose up -d influxdb


while [ $(curl -sI http://127.0.0.1:8086/ping | grep "X-Influxdb-Version" | wc -l) == 0 ]
do
    echo -e "Wait for service ready..."
    sleep 10
done

sleep 10

echo -e "## Test authenticate connection"
curl -G "http://localhost:8086/query?u=admin&p=$INFLUXDB_ADMIN_PASSWORD" --data-urlencode "q=SHOW DATABASES"
sleep 5
curl -G "http://localhost:8086/query?u=homeassistant&p=$INFLUXDB_HOMEASSISTANT_PASSWORD" --data-urlencode "q=SHOW DATABASES"
