#!/usr/bin/env bash

echo -e "###########################################################################################"

if [[ $# -ne 2 ]]
then
  echo "Sintassi: $(basename $0) <configuration file .ini> <grafana admin passwd>"
  exit 1
fi

export INIFILE=$1
export GRAFANA_ADMIN_PASSWORD=$2


if [[ -s $INIFILE ]] ; then
    . $INIFILE
    echo "# Loaded configuration file: $INIFILE"
else
    echo "ERRORE: file di inizializzazione $INIFILE non trovato oppure vuoto!"
    exit 1
fi

echo -e "## Starting pull grafana"
./dockerc-pull.sh $INIFILE "" "" "" grafana

echo -e "## Putting up node-red"
docker-compose up -d grafana